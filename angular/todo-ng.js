'use strict';

var app = angular.module('todo', []);
app.controller('TodoController', TodoController);

function TodoController($scope, $http) {
	$scope.newTask = "my first task";
	$scope.submitTask = function() {
		console.log('submitting task', $scope.newTask);
		$scope.tasks.push({name: $scope.newTask});
	}

	$scope.getCompletedTasks = function() {
		if ($scope.tasks === undefined) return 0;
		var completedTasks = $scope.tasks.filter(function (task) {
			return task.completed;
		});
		return completedTasks.length;
	}

	$scope.selectAll=function(){

		console.log('selectAll() invoked');
		/*var len=$scope.getCompletedTasks();
		for(var i=0;i<len;i++)
			$scope.task.value.completed=true;
		*/

		angular.forEach($scope.tasks,function(value,index){
			value.completed=true;
		});
	}

	$scope.init = function() {
		var request = {
			method: 'GET',
			url: '/tasks',
		}
		$http(request)
			.success(function(data) {
				$scope.tasks = data;
			})
			.error(function(data) {
				console.error(data);
			});
	}

	$scope.archive = function(){
		console.log('archiving');
		var req=
		{
			method:'POST',
			url:'/tasks',
			data: { tasks:$scope.tasks}
		}
		$http(req)
		.success(function(){
			console.log('archiving done');
			})
		.error(function(){
			console.log("Error");
		});

	}

}
