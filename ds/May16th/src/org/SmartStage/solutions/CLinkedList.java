package org.SmartStage.solutions;

public class CLinkedList {

	private CLLNode head;
	private int size;
	
	public CLinkedList()
	{	head.setNext(head);;
		size = 0;
	}
	
	public CLinkedList(int data)
	{
		CLLNode temp = new CLLNode(data);
		head=temp;
		head.setNext(head);
		size = 1;
	}

	
	/*public CLinkedList(CLLNode head)
	{	this.head = head;
		head.setNext(head)
		size = 1;
	}
*/
	public CLLNode getHead() {
		return head;
	}
	public void setHead(CLLNode head) {
		this.head = head;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	//Write the implementation for the following methods.
	
	
	//#0. Prints all elements in the list.
	public void traverse() {
		CLLNode temp=head;
		System.out.print(head.getData()+" ");
		//temp=head.getNext();
		while(temp.getNext()!=head)
		{
			temp=temp.getNext();
			System.out.print(temp.getData()+ " ");
			
		}
		System.out.println("");
		

	}
	
	//#1. Returns the number of elements in the list.
	public int size() {
		
		return 0;
	}
	
	//#2. Returns true if the list contains zero elements, else return false.
	public boolean isEmpty() {
		if(head==null)
			return true;
		else
			return false;
	}
	
	//#3. Adds the element 'e' to the beginning of the list.
	public void addFirst(int e) {
		CLLNode n=new CLLNode(e);
		
		CLLNode temp=head;
		while(temp.getNext()!=head)
		{
			temp=temp.getNext();
		}
		//temp=temp.getNext();
		temp.setNext(n);
		n.setNext(head);
		head=n;
		
	}
	
	//#4. Adds the element 'e' to the end of the list.
	public void addLast(int e) {
		CLLNode m=new CLLNode(e);
		CLLNode temp=head;
		while(temp.getNext()!=head)
		{
			temp=temp.getNext();
		}
		temp.setNext(m);
		m.setNext(head);

	}
	
	//#5. Adds the element 'e' at the ith position in the list.
	public void add(int e, int i) {
		CLLNode m=new CLLNode(e);
		int count=1;
		CLLNode temp=head;
		

	}
	
	//#6. Returns the index for first occurrence of element 'e' in the list.
	public int firstIndexOf(int e) {

		return 0;
	}
	
	//#6. Returns the index for last occurrence of element 'e' in the list.
	public int lastIndexOf(int e) {

		return 0;
	}
		
	//#8. Returns true if the element 'e' is present in the list, else return false.
	public boolean contains(int e) {

		return false;
	}
	
	//#9. Returns the element which occurs at ith position in the list (but doesn't remove it).
	public int get(int i) {

		return 0;
	}
	
	//#10. Returns the first element in the list (but doesn't remove it).
	public int getFirst() {

		return 0;
	}
	
	//#11. Returns the last element in the list (but doesn't remove it).
	public int getLast() {

		return 0;
	}
	
	//#12. Removes the element which occurs at ith position in the list.
	public void remove(int i) {

	}
	
	//#13. Removes the first element in the list.
	public void removeFirst() {

	}
	
	//#14. Removes the last element in the list.
	public void removeLast() {
		CLLNode temp=head;
		if(head.getNext()==head || head==null)
			head=null;
		else
		{
			while(temp.getNext().getNext()!=head)
			{
				temp=temp.getNext();
			}
			temp.setNext(head);
		}

	}
	
	//#15. Removes the first occurrence of element 'e' in the list.
	public void removeFirstOccurrence() {

	}
	
	//#16. Removes the last occurrence of element 'e' in the list.
	public void removeLastOccurrence() {

	}
	
	//#17. Removes all the elements in the list.
	public void removeAll() {

	}
	
	//#18. Replaces the element at ith position in the list with element 'e'.
	public void set(int e, int i) {

	}
	
}
