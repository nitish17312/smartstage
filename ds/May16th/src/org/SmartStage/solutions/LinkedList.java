package org.SmartStage.solutions;

public class LinkedList {
	
	private SLLNode head;
	private int size;
	
	public LinkedList(int arr[])
	{
		for(int i=arr.length-1;i>=0;i--)
			addFirst(arr[i]);
	}
	
	public LinkedList(LinkedList t)
	{
		//head=t.head;
		this.setHead(t.getHead());
	}
	public LinkedList()
	{	head = null;
		size = 0;
	}
	
	/*public LinkedList(LinkedList list1)
	{
		SLLNode head=list1.getHead();
		addFirst(head.getData());
		while(head.getNext()!=null)
		{
			head=head.getNext();
			addFirst(head.getData());
		}
	}*/
	
	public LinkedList(int data)
	{
		SLLNode temp = new SLLNode(data);
		head=temp;
		size = 1;
	}

	public SLLNode getHead() {
		return head;
	}
	public void setHead(SLLNode head) {
		this.head = head;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	//Write the implementation for the following methods.
	
	
	//#0. Prints all elements in the list.
	public void traverse() {
		
		SLLNode temp = head;
		 System.out.println("#####################################");
		 if(temp==null)
		 {			 
			System.out.println("empty");			 
		 }
		 else
		 {
			System.out.print(temp.getData()+" ");
			 while(temp.getNext()!=null)
			 {
				 temp=temp.getNext();
				 System.out.print(temp.getData()+" ");
				 
			 }
		 }
		 System.out.println(" ");
		 System.out.println("#####################################");	
	}
	
	//#1. Returns the number of elements in the list.
	public int size() {
		
		SLLNode temp=head;
		int count=1;
		if(temp==null)
			return -1;
		else
		{
			while(temp.getNext()!=null)
			{
				count++;
				temp=temp.getNext();
				
			}
			return count;
		}
	
		/*if(head==null)                      // ran implem
		return 0;
		else
		{
			int l=1;
			
			SLLNode temp=head;
			while(temp.getNext()!=null)
			{
				l++;
				temp=temp.getNext();
			}
			return l;
		}*/
	}
	
	//#2. Returns true if the list contains zero elements, else return false.
	public boolean isEmpty() {
		if(head==null)
			return true;
		return false;
	}
	
	//#3. Adds the element 'e' to the beginning of the list.
	public void addFirst(int e) {
		SLLNode n=new SLLNode(e);
		n.setNext(head);
		head=n;

	}
	
	//#4. Adds the element 'e' to the end of the list.
	public void addLast(int e) {
		SLLNode temp=head;
		if(temp==null)
		{
			head=new SLLNode(e);
		}
		else
		{
			while(temp.getNext()!=null)
			{
				temp=temp.getNext();
			}
			temp.setNext(new SLLNode(e));
		}

	}
	
	//#5. Adds the element 'e' at the ith position in the list.
	public void add(int e, int i) {
    	SLLNode temp=head;
    	SLLNode newNode=new SLLNode(e);
    	if(isEmpty())
    	{
    		this.addFirst(e);
    		System.out.println("here!!");
    	}
    	else if(i==1)
    		addFirst(e);
    	else
    	{
    	int count=1;
    	while(temp.getNext()!=null)
    	{
    		if(count==i-1)
    		{
    		newNode.setNext(temp.getNext());
    		temp.setNext(newNode);
    		System.out.println("here!! count is" +count);
    		break;
    		}
    		temp=temp.getNext();
    		count++;
    	}
    	}
		/*SLLNode temp = head;
		SLLNode m = new SLLNode(e); 	
		for (int k=0;k<i-1;k++)
			{
		   temp=temp.getNext();
		   	}
	     m=temp.getNext();
	    SLLNode n = new SLLNode(e);
	    temp.setNext(n);
	    n.setNext(m);*/
	}
	
	//#6. Returns the index for first occurrence of element 'e' in the list.
	public int firstIndexOf(int e) {
		SLLNode temp=head;
		int count=0;
		while((temp!=null) && (temp.getData()!=e))
		{
			temp=temp.getNext();
			count++;
		}
		if(temp==null)
		return -1;
		else return count;
	}
	
	//#7. Returns the index for last occurrence of element 'e' in the list.
	public int lastIndexOf(int e) {
		
		SLLNode temp=head;
		int count=1;
		int index=-1;
		if(temp==null)
			return -1;
		while(temp!=null)
		{
			if(temp.getData()==e)
				index=count;
			temp=temp.getNext();
			count++;			
		}
		return index;	
	}		
	//#8. Returns true if the element 'e' is present in the list, else return false.
	public boolean contains(int e) throws Exception {
		SLLNode temp=head;
		if(head==null)
			return false;
		else
			while(temp!=null)
			{
				if(temp.getData()==e)
					return true;
				temp=temp.getNext();
			}
		return false;
	}
	
	//#9. Returns the element which occurs at ith position in the list (but doesn't remove it).
	public int get(int i) throws Exception {
		SLLNode temp=head;
		int count =1;
		if(temp==null)
			throw new Exception();
		else
			while(temp!=null)
			{
				if(count ==i)
					return temp.getData();
				temp=temp.getNext();
				count++;
			}
		return -1;
	/*	SLLNode temp=head;
		if(size()<i || temp==null)
			return -1;
		else
			for(int k=0;k<i;k++)
			{
				temp=temp.getNext();
			}
		return temp.getData();*/
	}
	
	//#10. Returns the first element in the list (but doesn't remove it).
	public int getFirst() throws Exception {
		SLLNode temp=head;
		if(temp==null)
			throw new Exception();
		else return temp.getData();
				
	}
	
	//#11. Returns the last element in the list (but doesn't remove it).
	public int getLast() throws Exception {
		SLLNode temp=head;
		if(temp==null)
			throw new Exception();
		else
		{
			while(temp.getNext()!=null)
			{
				temp=temp.getNext();
			}
			return temp.getData();
		}
	}
	
	//#12. Removes the element which occurs at ith position in the list.  //unable to remove the last:pls edit
	public void remove(int i) throws Exception {
		System.out.println("Removing at ith position");
		SLLNode temp;
		temp=head;
		if(temp==null)
			throw new Exception();
		else if(i==1)
		{
			removeFirst();
		}
		else
		{
			int count=1;
			while(temp!=null && count<i)
			{
				if(count==i-1)
				{
					SLLNode temp2=temp.getNext();
					temp.setNext(temp2.getNext());
				}
				count++;
				temp=temp.getNext();
			}
		}
	}
	
	//#13. Removes the first element in the list.
	public void removeFirst() throws Exception {
		SLLNode temp=head;
		if(temp==null)
			throw new Exception();
		else
		{
			temp=temp.getNext();
			head=temp;
		}

	}
	
	//#14. Removes the last element in the list.
	public void removeLast() throws Exception {
		SLLNode temp=head;
		if(temp.getNext()==null)
		{
			throw new Exception();
		}
		else
		{
			while((temp.getNext()).getNext()!=null)
			{
				temp=temp.getNext();
			}
			temp.setNext(null);
		}

	}
	
	//#15. Removes the first occurrence of element 'e' in the list.
	public void removeFirstOccurrence(int e) throws Exception{
		
		SLLNode temp;
		SLLNode m;
		temp=head;
		if(temp==null)
			throw new Exception();
		else if(temp.getData()==e)
			head=temp.getNext();
		else
		{
			int flag=0;
			while(temp.getNext()!=null)
			{
				if((temp.getNext()).getData()==e)
				{
					flag=1;
					break;
				}
				temp=temp.getNext();
			}
			if(flag==1)
			{
			m=(temp.getNext()).getNext();
			temp.setNext(m);
			}						
		}
		
		
		/*SLLNode temp;
		temp=head;
		if(temp==null)
			throw new Exception();
		if(temp.getData()==e)
			head=temp.getNext();
		else
		{
			while(temp.getNext()!=null)
			{
				if((temp.getNext()).getData()==e)
				{
					if((temp.getNext()).getNext()==null)
						temp.setNext(null);
					else
					temp.setNext((temp.getNext()).getNext());
				}
				temp=temp.getNext();
			}
		}*/
		
		/*SLLNode temp;
		if(head==null)
		{
			throw new Exception();
		}
		else
		{
			temp=head;
		}
		while(temp.getNext()!=null)
		{
			if((temp.getNext()).getData()==e)
			{
				temp.setNext((temp.getNext()).getNext());
			}
		}
*/
	}
	
	//#16. Removes the last occurrence of element 'e' in the list.
	public void removeLastOccurrence(int e) throws Exception{
		
		SLLNode temp;
		SLLNode prev=null;
		SLLNode m;
		temp=head;
		int flag=0;
		if(temp==null)
			throw new Exception();
		else if(temp.getData()==e && temp.getNext()==null)
			head=null;
		else if(temp.getData()==e)
		{
			flag=1;
			System.out.println("hey!!");
		}
		else
		{
			while(temp.getNext()!=null)
			{
				if((temp.getNext()).getData()==e)
				{
					prev=temp;
					flag=2;
					//System.out.println("addddddddddd");
				}
				temp=temp.getNext();				
			}
		}
			if(flag==1)
			{
				//System.out.println("addddddddddjlkafhgjlkdd");
				head=head.getNext();
			}
			else
			{
			m=(prev.getNext()).getNext();
			prev.setNext(m);
			}
			
	}
	
	//#17. Removes all the elements in the list.
	public void removeAll() {
		head=null;

	}
	
	//#18. Replaces the element at ith position in the list with element 'e'.
	public void set(int e, int i) throws Exception{
		SLLNode temp=head;
		if(i>this.size())
		{
			throw new Exception();
		}
		else
			for(int k=0;k<i-1;k++)
			{
				temp=temp.getNext();
			}
		temp.setData(e);
			
		}
	//#19.Reverse a linked list
	
	public void reverse()
	{
		if(head==null || head.getNext()==null)
			System.out.println("Nothing to reverse");
		else
		{
			SLLNode current,prev,next;
			current=head;
			prev=null;
			while(current!=null)
			{
				next=current.getNext();
				current.setNext(prev);
				prev=current;
				current=next;
			}
			head=prev;
		}
			
	}
	
	//#20.Reverse last k elements of linked list
	
	public void reverseLastK(int k)
	{
		int size=0;
		int count=1;
		SLLNode temp=head;
		while(temp!=null)
		{
			size++;
			temp=temp.getNext();
		}
		if(k>size){
			System.out.println("Operation not possible with this size");
		}
		//System.out.println(size);
		else if(k==size)
			reverse();
		else
		{
			temp=head;
			while(temp!=null && count<size-k)
			{
				temp=temp.getNext();
				count++;
			}
			//System.out.println("The curr tem is: "+temp.getData());
			SLLNode prev,current,next;
			current=temp.getNext();
			prev=null;
			while(current!=null)
			{
				next=current.getNext();
				current.setNext(prev);
				prev=current;
				current=next;
			}
			temp.setNext(prev);
		}
		
	}
	
	//#20.Swap two elements of the list
	public void swap(int a,int b)
	{
	SLLNode prev1=head,e1=null,prev2=head,e2=null;
	while((prev1.getNext()).getData()!=a && prev1.getNext()!=null)
	{
		prev1=prev1.getNext();
	}
	while((prev2.getNext()).getData()!=b && prev2.getNext()!=null)
	{
		prev2=prev2.getNext();
	}
	e1=prev1.getNext();
	e2=prev2.getNext();
	//reached position, now swap begins
	
	prev1.setNext(e1.getNext());
	prev2.setNext(e2.getNext());
	e1.setNext(prev2.getNext());
	e2.setNext(prev1.getNext());
	prev1.setNext(e2);
	prev2.setNext(e1);
	
	
	}
	
	
	//#21. Delete N nodes after M nodes
	public void nAfterM(int n,int m) throws Exception
	{
		SLLNode temp1=head,temp2=head;
		int count=1;
		if(head==null)
			throw new Exception();
		else if(head.getNext()==null && n==1 && m==0)
		{
			head=null;
		}		
		else if(head.getNext()==null && n!=1 && m!=0)
		{
			throw new Exception();
		}
		else if(head.getNext()!=null && n==1 && m==0)
		{
			head=head.getNext();
		}
		
		else
		{
			while(temp1.getNext()!=null && count<=m)
			{
				if(count==m)
				{
					temp2=temp1;
					break;
				}
				temp1=temp1.getNext();
				count++;
			}
			count=0;
			while(temp2.getNext()!=null && count<=n)
			{
				if(count==n)
				{
					break;
				}
				temp2=temp2.getNext();
				count++;
			}
			temp1.setNext(temp2.getNext());
		}
	}
	
	
	
	
}
	
