package org.SmartStage.solutions;

public class CirCaller {
	
	public static void sample1()
	{
		CLinkedList list1=new CLinkedList(1);
		list1.traverse();
		list1.addFirst(20);
		list1.addFirst(23);
		list1.addFirst(50);
		list1.addLast(2);
		list1.addLast(3);
		list1.addLast(4);
		
		list1.traverse();
		list1.removeLast();
		list1.traverse();
		
	}	
	public static void CLL2()
	{
		CLinkedList2 m=new CLinkedList2();
		m.addLast(6);
		m.addLast(7);
		m.addLast(8);
		m.addFirst(4);
		m.addFirst(3);
		m.removeFirst();
		m.removeLast();
		
		
		m.traverse();
	}
	public static void stackCall() throws Exception
	{
		Stack s=new Stack();
		s.push(1);
		s.push(2);
		s.push(3);
		System.out.println(s.peek());
		System.out.println(s.pop());
		System.out.println(s.peek());
		
		//s.pop();
		//s.peek();
		
		
		
	}
	public static void DLLCaller()
	{
		DLinkedList d1=new DLinkedList(55);
		d1.traverse();
		d1.addFirst(1);
		d1.addFirst(2);
		d1.addFirst(3);
		d1.traverse();
		d1.addLast(88);
		d1.addLast(95);
		d1.traverse();
		d1.removeFirst();
		d1.traverse();
		d1.removeLast();
		d1.traverse();
		d1.remove(3);
		d1.traverse();
		d1.traverseTnF();
		
		
	}
	public static void StackMCall() throws Exception
	{
		StackM m1=new StackM();
		m1.push(1);
		m1.push(2);
		m1.push(3);
		m1.push(4);
		m1.push(5);
		System.out.println(m1.peek());
		System.out.println(m1.getMid());
		m1.pop();
		System.out.println(m1.peek());
		System.out.println(m1.getMid());
		m1.pop();
		System.out.println(m1.peek());
		System.out.println(m1.getMid());
		m1.pop();
		m1.pop();
		System.out.println(m1.peek());
		System.out.println(m1.getMid());
		/*m1.pop();
		System.out.println(m1.peek());
		System.out.println(m1.getMid());
		*/
		
		/*System.out.println("Popped: "+m1.pop());
		System.out.println(m1.getMid());
		
		System.out.println("Popped: "+m1.pop());
		
		System.out.println(m1.getMid());
		*/
		
		
	}
	public static void main(String[] args) throws Exception{
		//sample1();
		//CLL2();
		//stackCall();
		//DLLCaller();
		StackMCall();
		// TODO Auto-generated method stub

	}

}
