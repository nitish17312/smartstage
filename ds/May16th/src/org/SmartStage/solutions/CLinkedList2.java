package org.SmartStage.solutions;

public class CLinkedList2 {
	
	private CLLNode2 head, rear;
	private int size;
	
	public CLinkedList2()
	{	head = null;
		rear = null;
		size = 0;
	}
	
	public CLinkedList2(int data)
	{
		head = new CLLNode2(data);
		rear = head;
		rear.setNext(head);
	}
	
	public CLinkedList2(CLLNode2 head)
	{	this.head = head;
		size = 1;
	}

	public CLLNode2 getHead() {
		return head;
	}
	public void setHead(CLLNode2 head) {
		this.head = head;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	//Write the implementation for the following methods.
	
	
	//#0. Prints all elements in the list.
	public void traverse() {
		if(head==null)
			System.out.println("Empty");
		else
		{
		CLLNode2 temp=head;
		System.out.print(head.getData()+" ");
		//temp=head.getNext();
		while(temp.getNext()!=head)
		{
			temp=temp.getNext();
			System.out.print(temp.getData()+ " ");
			
		}
		System.out.println("");
		System.out.println("##############################");
		
		
		}
	}	
	//#1. Returns the number of elements in the list.
	public int size() {
		
		return 0;
	}
	
	//#2. Returns true if the list contains zero elements, else return false.
	public boolean isEmpty() {
		
		return true;
	}
	
	//#3. Adds the element 'e' to the beginning of the list.
	public void addFirst(int e) {
		CLLNode2 m=new CLLNode2(e);
		rear.setNext(m);
		m.setNext(head);
		head=rear.getNext();
		

	}
	
	//#4. Adds the element 'e' to the end of the list.
	public void addLast(int e) {
		
		CLLNode2 temp=rear;
		CLLNode2 m=new CLLNode2(e);
		rear.setNext(m);
		m.setNext(head);
		rear=temp.getNext();

	}
	
	//#5. Adds the element 'e' at the ith position in the list.
	public void add(int e, int i) {


	}
	
	//#6. Returns the index for first occurrence of element 'e' in the list.
	public int firstIndexOf(int e) {

		return 0;
	}
	
	//#6. Returns the index for last occurrence of element 'e' in the list.
	public int lastIndexOf(int e) {

		return 0;
	}
		
	//#8. Returns true if the element 'e' is present in the list, else return false.
	public boolean contains(int e) {

		return false;
	}
	
	//#9. Returns the element which occurs at ith position in the list (but doesn't remove it).
	public int get(int i) {

		return 0;
	}
	
	//#10. Returns the first element in the list (but doesn't remove it).
	public int getFirst() {

		return 0;
	}
	
	//#11. Returns the last element in the list (but doesn't remove it).
	public int getLast() {

		return 0;
	}
	
	//#12. Removes the element which occurs at ith position in the list.
	public void remove(int i) {
		if(head==null)
			System.out.println("Empty");
		else if(head==rear)
		{
			head=null;
			rear=null;
		}
		else
		{
			CLLNode2 temp=head;
			int count=1;
			while(count<i-1 && temp!=rear)
			{
				temp=temp.getNext();
			}
			//temp.setNext(temp.getNext);
		}
			//System.out.println("Emptied")

	}
	
	//#13. Removes the first element in the list.
	public void removeFirst() {
		if(head==null)
			System.out.println("Empty");
		else if(head==rear)
		{
			head=null;
			rear=null;
		}
		else
		{
			rear.setNext(head.getNext());
			head=head.getNext();
		}

	}
	
	//#14. Removes the last element in the list.
	public void removeLast() {
		CLLNode2 temp=head;
		if(head==null)
			System.out.println("Empty");
		else if(head==rear)
		{
			head=null;
			rear=null;
		}
		else
		{
			while(temp.getNext()!=rear)
			{
				temp=temp.getNext();
			}
			temp.setNext(head);
			rear=temp;
		}
	}
	
	//#15. Removes the first occurrence of element 'e' in the list.
	public void removeFirstOccurrence() {

	}
	
	//#16. Removes the last occurrence of element 'e' in the list.
	public void removeLastOccurrence() {

	}
	
	//#17. Removes all the elements in the list.
	public void removeAll() {

	}
	
	//#18. Replaces the element at ith position in the list with element 'e'.
	public void set(int e, int i) {

	}
	
}
