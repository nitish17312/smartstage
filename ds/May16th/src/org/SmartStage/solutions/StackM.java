package org.SmartStage.solutions;

public class StackM {
	
	private StackNodeM head;
	private StackNodeM mid;
	private int count=0;
	//private int size;
	
	public StackM()
	{	head = null;
		mid=head;
	}
	
	public StackM(int a)
	{
		StackNodeM n=new StackNodeM(a);
		head=n;
		n.setNext(null);
	}
	public StackNodeM getHead() {
		return head;
	}
	public void setHead(StackNodeM head) {
		this.head = head;
	}
	
	public int getMid()
	{
		return mid.getData();
	}
	
	public int getCount()
	{
		return count;
	}
	public void setCount()
	{
		count++;
	}
	public void unsetCount()
	{
		count--;
	}
	
	/*public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	*/
	//Write the implementation for the following methods.
	
	//#1.Push
	public void push(int e)
	{
		StackNodeM n=new StackNodeM(e);
		if(head==null)
		{
			head=n;
			mid=head;
		}
		else
		{
		n.setNext(head);
		head.setPrev(n);
		n.setPrev(null);
		head=n;
		setCount();
		if(getCount()%2==0)
			mid=mid.getPrev();
	}
}
	
	//2.isEmpty
	public boolean isEmpty()
	{
		if(head==null)
			return true;
		else
			return false;
	}
	
	//#3.Pop
	public int pop() throws Exception 
	{
		if(isEmpty()==true)
		{
			throw new Exception();
		}
		else			
		{
			int temp=head.getData();
			unsetCount();
			head=head.getNext();
			head.setPrev(null);
			
			if(getCount()%2!=0)
			{
				mid=mid.getNext();
			}
			return temp;
		}
		
	}
	
	//4.Peek
	public int peek() throws Exception
	
	{
		if(isEmpty()==true)
			throw new Exception();
		else
			return head.getData();
		
	}
	
	
	
	/*//#0. Prints all elements in the list.
	public void traverse() {

	}
	
	//#1. Returns the number of elements in the list.
	public int size() {
		
		return 0;
	}
	
	//#2. Returns true if the list contains zero elements, else return false.
	public boolean isEmpty() {
		
		return true;
	}
	
	//#3. Adds the element 'e' to the beginning of the list.
	public void addFirst(int e) {

	}
	
	//#4. Adds the element 'e' to the end of the list.
	public void addLast(int e) {

	}
	
	//#5. Adds the element 'e' at the ith position in the list.
	public void add(int e, int i) {

	}
	
	//#6. Returns the index for first occurrence of element 'e' in the list.
	public int firstIndexOf(int e) {

		return 0;
	}
	
	//#6. Returns the index for last occurrence of element 'e' in the list.
	public int lastIndexOf(int e) {

		return 0;
	}
		
	//#8. Returns true if the element 'e' is present in the list, else return false.
	public boolean contains(int e) {

		return false;
	}
	
	//#9. Returns the element which occurs at ith position in the list (but doesn't remove it).
	public int get(int i) {

		return 0;
	}
	
	//#10. Returns the first element in the list (but doesn't remove it).
	public int getFirst() {

		return 0;
	}
	
	//#11. Returns the last element in the list (but doesn't remove it).
	public int getLast() {

		return 0;
	}
	
	//#12. Removes the element which occurs at ith position in the list.
	public void remove(int i) {

	}
	
	//#13. Removes the first element in the list.
	public void removeFirst() {

	}
	
	//#14. Removes the last element in the list.
	public void removeLast() {

	}
	
	//#15. Removes the first occurrence of element 'e' in the list.
	public void removeFirstOccurrence() {

	}
	
	//#16. Removes the last occurrence of element 'e' in the list.
	public void removeLastOccurrence() {

	}
	
	//#17. Removes all the elements in the list.
	public void removeAll() {

	}
	
	//#18. Replaces the element at ith position in the list with element 'e'.
	public void set(int e, int i) {

	}
*/	
}
