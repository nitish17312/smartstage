package org.SmartStage.solutions;

public class StackNodeM {

	private int data;
	private StackNodeM next;
	private StackNodeM prev;
	
	public StackNodeM (int data) {
		this.data=data;
		this.next=null;
		this.prev=null;
	}
	
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	public StackNodeM getNext() {
		return next;
	}
	public StackNodeM getPrev()
	{
		return prev;
	}
	public void setNext(StackNodeM next) {
		this.next = next;
	}
	public void setPrev(StackNodeM prev)
	{
		this.prev=prev;
	}
}		

