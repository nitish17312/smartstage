package org.SmartStage.solutions;

public class CLLNode2 {

	private int data;
	private CLLNode2 next;
	
	public CLLNode2 (int data) {
		this.data=data;
		this.next=null;
	}
	
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	public CLLNode2 getNext() {
		return next;
	}
	public void setNext(CLLNode2 next) {
		this.next = next;
	}
		
}
