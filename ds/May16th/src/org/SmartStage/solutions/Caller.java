package org.SmartStage.solutions;

public class Caller {
	
	
	public static void callMeth() throws Exception
	{
		LinkedList list1=new LinkedList();
		
		//add first
		for(int i=5;i>0;i--)
			list1.addFirst(i);
		
		//0.traverse
		list1.traverse();
		
		//1.size
		System.out.println("The current size of the list is: " +list1.size());
		
		//2.isEmpty
		boolean flag1=list1.isEmpty();
		if(flag1==true)
			System.out.println("The list is empty");
		else
			System.out.println("The list is not empty");
		
		//3.add first :already done at top
		
		//4.add last   :also traverse again and check size again
		for(int i=6;i<=8;i++)
			list1.addLast(i);
		list1.traverse();
		System.out.println("The current size of the list is: " +list1.size());
		
		//5.add element to the ith position in the list
		list1.add(100, 5);
		list1.traverse();
		
		//6.first occurance of e
		System.out.println("The first occurance of 100 is at : " +list1.firstIndexOf(100));
		System.out.println("The first occurance of 200 is at : " +list1.firstIndexOf(200));
		
		//7.last occurrence of e
		System.out.println("The last occurance of 100 is at : " +list1.lastIndexOf(100));
		System.out.println("The last occurance of 200 is at : " +list1.lastIndexOf(200));
		
		//8.if e is present in the list
		boolean flag2=list1.contains(100);
		if(flag2==true)
			System.out.println("Contains 100");
		else
			System.out.println("Doesn't contain 100");
		
		//9.Element at ith position
		System.out.println("Element at 5th position is: "+list1.get(5));
		
		//10.Read the first element
		System.out.println("Element at first position is "+list1.getFirst());
		
		//11.Read the last element
		System.out.println("The element at the last postion is: "+list1.getLast());
		
		//12.Remove the element at the ith position in the list
		list1.traverse();
		list1.remove(3);
		list1.traverse();
		
		//13.remove the first element in the list
		list1.removeFirst();
		System.out.println("After removing the first");
		list1.traverse();
		
		//14.Remove the last
		list1.traverse();
		System.out.println("After removing at last");
		list1.removeLast();
		list1.traverse();
		
		//15.Remove first occurrence of e
		list1.removeFirstOccurrence(785);
		System.out.println("After removing first occurance of 100");
		list1.traverse();
		
		//16.Remove last occurrence of e
		System.out.println("After removing the last occurrence of 4");
		list1.removeLastOccurrence(100);
		list1.traverse();
		
		//17.Remove all elements in the list
		/*list1.removeAll();
		list1.traverse();*/
		
		//18.Replace element at ith postion with e
		list1.set(9999, 1);
		list1.traverse();
		
		//19.Reverse the list
		list1.reverse();
		list1.traverse();
		
		//20.Reverse last k elements in a Linked List
		list1.reverseLastK(3);
		list1.traverse();
		
		//#20.Swap two elements
		//list1.swap(4,6);
		//list1.traverse();
		
		//#21.Delete N nodes after N Nodes
		//list1.nAfterM(2, 2);
		//list1.nAfterM(0, 0);
		//list1.nAfterM(1, 0);
		//list1.nAfterM(1, 1);
		//list1.nAfterM(4, 1);
		//list1.traverse();
		  
		
	}
	
	public static void tempTest() throws Exception
	{
		LinkedList list2=new LinkedList();
		list2.addFirst(500);
		list2.removeFirstOccurrence(5005);
		list2.traverse();
	}
	
	public static void LLT()
	{
		LinkedList ll1=new LinkedList();
		for(int i=1;i<=20;i++)
		{
		ll1.addLast(i);
		}
		
		LinkedList ll2=new LinkedList(ll1);
		ll2.traverse();
	}	
	public static void main(String[] args) throws Exception{  // throws Exception can be written here instead of  the try catch block
		callMeth();
		//tempTest();
		//LLT();
	}
}
