package org.SmartStage.solutions;

public class DLinkedList {
	
	private DLLNode head;
	private int size;
	
	public DLinkedList()
	{	head = null;
		size = 0;
	}
	
	public DLinkedList(int a)
	{	
		head=new DLLNode(a);
		head.setNext(null);
		head.setPrev(null);
	}

	public DLLNode getHead() {
		return head;
	}
	public void setHead(DLLNode head) {
		this.head = head;
	}
	/*public int getSize() {
		return size;
	}
	*/
	/*public void setSize(int size) {
		this.size = size;
	}
	*/
	//Write the implementation for the following methods.
	
	
	//#0. Prints all elements in the list.
	public void traverse() {
		if(head==null)
		{
			System.out.println("Empty");
		}
		else
		{
			DLLNode temp=head;
			while(temp.getNext()!=null)
			{
				System.out.print(temp.getData()+ " ");
				temp=temp.getNext();
			}
			System.out.println(temp.getData());
			
		}
		System.out.println("##############################");

	}
	
	//#1. Returns the number of elements in the list.
	/*public int size() {
		
		return 0;
	}*/
	
	//#2. Returns true if the list contains zero elements, else return false.
	public boolean isEmpty() {
		if(head==null)
		return true;
		else
			return false;
	}
	
	//#3. Adds the element 'e' to the beginning of the list.
	public void addFirst(int e) {
		DLLNode m=new DLLNode(e);
		if(head==null)
		{
			head=m;
			m.setNext(null);
			m.setPrev(null);			
		}
		else
		{
			m.setNext(head);
			head.setPrev(m);
			head=m;
		}
	}
	
	//#4. Adds the element 'e' to the end of the list.
	public void addLast(int e) {
		
		DLLNode m=new DLLNode(e);
		if(head==null)
		{
			head=m;
			m.setNext(null);
			m.setPrev(null);			
		}
		else
		{
			DLLNode temp=head;
			while(temp.getNext()!=null)
			{
				temp=temp.getNext();
			}
			//temp=temp.getNext();
			temp.setNext(m);
			m.setPrev(temp);
			m.setNext(null);
		}

	}
	
	//#5. Adds the element 'e' at the ith position in the list.
	public void add(int e, int i) {
		DLLNode m=new DLLNode(e);
		if(head==null && i==1)
		{

		}

	}
	
	//#6. Returns the index for first occurrence of element 'e' in the list.
	public int firstIndexOf(int e) {

		return 0;
	}
	
	//#6. Returns the index for last occurrence of element 'e' in the list.
	public int lastIndexOf(int e) {

		return 0;
	}
		
	//#8. Returns true if the element 'e' is present in the list, else return false.
	public boolean contains(int e) {

		return false;
	}
	
	//#9. Returns the element which occurs at ith position in the list (but doesn't remove it).
	public int get(int i) {

		return 0;
	}
	
	//#10. Returns the first element in the list (but doesn't remove it).
	public int getFirst() {

		return 0;
	}
	
	//#11. Returns the last element in the list (but doesn't remove it).
	public int getLast() {

		return 0;
	}
	
	//#12. Removes the element which occurs at ith position in the list.
	public void remove(int i) {
		if(head==null)
			System.out.println("Empty");
		else
		{
			DLLNode temp=head;
			int flag=0;
			for(int k=1;k<i;k++)
			{
				if(temp.getNext()==null)
				{
					flag=1;
					break;
				}
				temp=temp.getNext();
			}
			temp.getPrev().setNext(temp.getNext());
			temp.getNext().setPrev(temp.getPrev());
		}
			
	}
	
	//#13. Removes the first element in the list.
	public void removeFirst() {
		if(head==null)
			System.out.println("Empty");
		else if(head.getNext()==null)
			head=null;
		else
		{
			head=head.getNext();
			head.setPrev(null);
		}

	}
	
	//#14. Removes the last element in the list.
	public void removeLast() {
		
		if(head==null)
			System.out.println("Empty");
		else if(head.getNext()==null)
			head=null;
		else
		{
			DLLNode temp=head;
			while(temp.getNext()!=null)
			{
				temp=temp.getNext();
			}
			temp=temp.getPrev();
			temp.setNext(null);
		}

	}
	
	//#15. Removes the first occurrence of element 'e' in the list.
	public void removeFirstOccurrence() {

	}
	
	//#16. Removes the last occurrence of element 'e' in the list.
	public void removeLastOccurrence() {

	}
	
	//#17. Removes all the elements in the list.
	public void removeAll() {

	}
	
	//#18. Replaces the element at ith position in the list with element 'e'.
	public void set(int e, int i) {

	}
	
	//19. To and Fro
	public void traverseTnF()
	{
		if(head==null)
		{
			System.out.println("Empty");
		}
		else
		{
			DLLNode temp=head;
			while(temp.getNext()!=null)
			{
				System.out.print(temp.getData()+ " ");
				temp=temp.getNext();
			}
			System.out.print(temp.getData()+" ");
			while(temp.getPrev()!=null)
			{
				System.out.print(temp.getData()+ " ");
				temp=temp.getPrev();
			}
			//temp=temp.getPrev();
			System.out.println(temp.getData()+" ");		
		}
		System.out.println("##############################");
	}
}
